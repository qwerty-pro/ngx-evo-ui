import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EvoInputModule } from '@evo-kit';
import { SharedModule } from '../../shared';
import { InputRoutingModule } from './input-routing.module';
import { InputComponent } from './input.component';


@NgModule({
  declarations: [
    InputComponent
  ],
  imports: [
    CommonModule,
    EvoInputModule,
    FormsModule,
    InputRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class InputModule { }
