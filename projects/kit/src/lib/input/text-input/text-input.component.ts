import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { debounceTime, distinctUntilChanged, fromEvent, Subscription } from 'rxjs';
import { EvoUISizes, UtilsService } from '../../@utils';
import { TextInputTypes } from '../input.models';

@Component({
  selector: 'evo-text-input',
  template: `
    <ng-container *ngIf="prefix">
      <evo-icon class="evo-text-input__icon evo-text-input__prefix-icon" [name]="prefix"></evo-icon>
    </ng-container>
    <ng-container *ngIf="!passwordMode && suffix">
      <evo-icon class="evo-text-input__icon evo-text-input__suffix-icon" [name]="suffix"></evo-icon>
    </ng-container>
    <ng-container *ngIf="passwordMode && !suffix">
      <evo-icon class="evo-text-input__icon evo-text-input__suffix-icon"
                [name]="isShowingPassword ? 'eyeOff' : 'eye'"
                (click)="toggleType()">
      </evo-icon>
    </ng-container>
    <input #input class="evo-text-input__input"
           [(ngModel)]="model"
           [disabled]="disabled"
           [placeholder]="placeholder"
           [readOnly]="readonly"
           [required]="required"
           [type]="type"
           (blur)="onBlur()"
    >
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextInputComponent),
      multi: true
    }
  ]
})
export class TextInputComponent implements ControlValueAccessor, OnInit {
  @HostBinding('class.evo-text-input') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() passwordMode: boolean = false;
  @Input() readonly: boolean = false;
  @Input() required: boolean = false;
  @Input() searchMode: boolean = false;
  @Input() placeholder: string = '';
  @Input() prefix: string = '';
  @Input() suffix: string = '';
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input()
  set model(value: string) {
    this._value = value;
  }
  
  get model(): string {
    return this._value;
  }
  
  @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('input', { static: true })
  private input: ElementRef<HTMLInputElement> = new ElementRef<HTMLInputElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses() {
    return `evo-input-${this._utilsService.getSizeClass(this.size)}`;
  }

  private _value: string = '';
  private _onChange: (value: string) => void = () => {};
  private _onTouched: () => void = () => {};

  public type: TextInputTypes = TextInputTypes.TEXT;
  public isShowingPassword: boolean = false;

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    if (this.passwordMode) {
      this.type = TextInputTypes.PASSWORD;
      this.isShowingPassword = false;
    }
  }

  writeValue(value: string): void {
    this._value = value;
  }
  
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  toggleType(): void {
    this.type = this.type === TextInputTypes.PASSWORD ? TextInputTypes.TEXT : TextInputTypes.PASSWORD;
    this.isShowingPassword = this.type === TextInputTypes.TEXT;
  }
  
  onBlur(): void {
    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
