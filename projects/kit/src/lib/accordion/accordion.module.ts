import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoIconModule } from '../icon';
import { AccordionComponent } from './accordion.component';
import { AccordionPanelComponent } from './accordion-panel.component';


const declarations: any[] = [AccordionComponent, AccordionPanelComponent];

@NgModule({
  declarations,
  imports: [CommonModule, EvoIconModule],
  exports: [...declarations]
})
export class EvoAccordionModule {}
