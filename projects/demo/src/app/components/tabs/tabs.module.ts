import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoTabsModule } from '@evo-kit';
import { SharedModule } from '../../shared';

import { TabsRoutingModule } from './tabs-routing.module';
import { TabsComponent } from './tabs.component';


@NgModule({
  declarations: [
    TabsComponent
  ],
  imports: [
    CommonModule,
    EvoTabsModule,
    SharedModule,
    TabsRoutingModule
  ]
})
export class TabsModule { }
