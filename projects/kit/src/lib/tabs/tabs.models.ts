export enum EvoTabsBarPosition {
  TOP = 'top',
  BOTTOM = 'bottom',
  LEFT = 'left',
  RIGHT = 'right'
}

export enum EvoTabsType {
  BLOCK = 'block',
  ROUNDED = 'rounded',
  CARD = 'card'
}
