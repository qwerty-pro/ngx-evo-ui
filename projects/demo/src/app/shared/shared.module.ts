import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ComponentPreviewComponent } from './component-preview/component-preview.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { PageContentComponent } from './page-content/page-content.component';
import { PageHeaderComponent } from './page-header/page-header.component';

const declarations: any[] = [ComponentPreviewComponent, LeftNavComponent, PageContentComponent, PageHeaderComponent];

@NgModule({
  declarations,
  imports: [CommonModule, RouterModule],
  exports: [...declarations]
})
export class SharedModule {}
