import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import { slideInOutAnimation } from '../@utils';

@Component({
  selector: 'evo-accordion-panel',
  template: `
    <button class="evo-accordion-panel__trigger" [class.active]="active" [disabled]="disabled" (click)="toggle()">
      <span class="evo-accordion-panel__header" *ngIf="!headerTmpl">{{ header }}</span>
      <ng-container *ngIf="headerTmpl" [ngTemplateOutlet]="headerTmpl"></ng-container>
      <evo-icon
        class="evo-accordion-panel__icon"
        name="expandMore"
        [style.transform]="active ? 'rotate(0deg)' : 'rotate(-90deg)'"
      ></evo-icon>
    </button>
    <div class="evo-accordion-panel__content-wrapper" [@slideInOut]="contentState">
      <div class="evo-accordion-panel__content">
        <span *ngIf="!contentTmpl">{{ content }}</span>
        <ng-container *ngIf="contentTmpl" [ngTemplateOutlet]="contentTmpl"></ng-container>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideInOutAnimation]
})
export class AccordionPanelComponent implements OnInit, OnChanges {
  @HostBinding('class.evo-accordion-panel') private readonly baseCss: boolean = true;
  
  @Output() panelToggle: EventEmitter<void> = new EventEmitter<void>();
  @Input() active: boolean = false;
  @Input() content: TemplateRef<void> | string = '';
  @Input() header: TemplateRef<void> | string = '';
  @Input() disabled: boolean = false;
  @Input() borderlessMode: boolean = false;
  
  @HostBinding('class.borderless') get isBorderless() {
    return this.isBorderlessMode;
  };

  headerTmpl: TemplateRef<any> | null = null;
  contentTmpl: TemplateRef<any> | null = null;
  contentState: string = '';
  isBorderlessMode: boolean = false;
  
  ngOnInit(): void {
    if (this.header instanceof TemplateRef) {
      this.headerTmpl = this.header;
    }

    if (this.content instanceof TemplateRef) {
      this.contentTmpl = this.content;
    }
  
    this.contentState = this.active ? 'in' : 'out';
    this.isBorderlessMode = this.borderlessMode;
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (!changes['active'].isFirstChange() && !this.disabled) {
      this.contentState = this.active ? 'in' : 'out'
    }
  }
  
  toggle(): void {
    this.panelToggle.emit();
  }
}
