import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { EvoIconModule } from '../icon';

import { NumberInputComponent } from './number-input/number-input.component';
import { TextareaAutosizeDirective } from './textarea/textarea-autosize.directive';
import { TextareaComponent } from './textarea/textarea.component';
import { TextInputComponent } from './text-input/text-input.component';


const declarations: any[] = [
  NumberInputComponent,
  TextareaAutosizeDirective,
  TextareaComponent,
  TextInputComponent
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    EvoIconModule,
    FormsModule,
    NgxMaskDirective,
    NgxMaskPipe,
    ReactiveFormsModule
  ],
  providers: [
    provideNgxMask()
  ],
  exports: [...declarations]
})
export class EvoInputModule { }
