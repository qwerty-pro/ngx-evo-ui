export interface EvoIcon {
  data: string | null;
  url: string;
}

export type EvoIconStore = Map<string, EvoIcon>;
