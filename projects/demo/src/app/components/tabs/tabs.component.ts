import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { EvoTabsBarPosition, EvoTabsType, EvoUISizes } from '@evo-kit';

type ITabItem = {
  label: string,
  active: boolean,
  disabled: boolean,
  link: any,
  iconName: string | null,
  content: string | null
};

@Component({
  selector: 'app-tabs',
  template: `
    <app-page-header pageTitle="Tabs"></app-page-header>
    <app-page-content [leftSectionTmpl]="leftSectionTmpl" [rightSectionTmpl]="rightSectionTmpl"></app-page-content>
    
    <ng-template #leftSectionTmpl>
      <app-component-preview name="Basic" description="Basic usage of tabs">
        <evo-tabs-bar>
          <evo-tab-item *ngFor="let tab of tabs1"
                        [active]="tab.active"
                        [label]="tab.label">
            <ng-container
              [ngTemplateOutlet]="tabContentTmpl"
              [ngTemplateOutletContext]="{ $implicit: tab }"
            ></ng-container>
          </evo-tab-item>
        </evo-tabs-bar>
      </app-component-preview>
      
      <app-component-preview name="Disabled Tabs" description="Usage tabs with disabled mode">
        <evo-tabs-bar>
          <evo-tab-item *ngFor="let tab of tabs3"
                        [label]="tab.label"
                        [active]="tab.active"
                        [disabled]="tab.disabled">
            <ng-container
              [ngTemplateOutlet]="tabContentTmpl"
              [ngTemplateOutletContext]="{ $implicit: tab }"
            ></ng-container>
          </evo-tab-item>
        </evo-tabs-bar>
      </app-component-preview>
      
      <app-component-preview name="Tabs type"
                             description="Tabs types: block (default), rounded, card">
        <evo-tabs-bar [type]="tabsType">
          <evo-tab-item *ngFor="let tab of tabs5"
                        [label]="tab.label"
                        [active]="tab.active"
                        [disabled]="tab.disabled">
            <ng-container
              [ngTemplateOutlet]="tabContentTmpl"
              [ngTemplateOutletContext]="{ $implicit: tab }"
            ></ng-container>
          </evo-tab-item>
        </evo-tabs-bar>
      </app-component-preview>
    </ng-template>

    <ng-template #rightSectionTmpl>
      <app-component-preview
        name="Icon"
        description="The tab with icon">
        <evo-tabs-bar>
          <evo-tab-item *ngFor="let tab of tabs2"
                        [active]="tab.active"
                        [iconName]="tab.iconName"
                        [label]="tab.label">
            <ng-container
              [ngTemplateOutlet]="tabContentTmpl"
              [ngTemplateOutletContext]="{ $implicit: tab }"
            ></ng-container>
          </evo-tab-item>
        </evo-tabs-bar>
      </app-component-preview>
      
      <app-component-preview name="Tabs bar position"
                             description="Tabs bar position: top (default), right, bottom, left">
        <evo-tabs-bar [position]="tabsBarPosition">
          <evo-tab-item *ngFor="let tab of tabs4"
                        [label]="tab.label"
                        [active]="tab.active">
            <ng-container
              [ngTemplateOutlet]="tabContentTmpl"
              [ngTemplateOutletContext]="{ $implicit: tab }"
            ></ng-container>
          </evo-tab-item>
        </evo-tabs-bar>
      </app-component-preview>
      
      <app-component-preview name="Tab size"
                             description="Tab sizes: small, medium (default), large">
        <evo-tabs-bar [position]="tabsBarPosition" [type]="tabsType" [size]="tabsSize">
          <evo-tab-item *ngFor="let tab of tabs6"
                        [label]="tab.label"
                        [active]="tab.active">
            <ng-container
              [ngTemplateOutlet]="tabContentTmpl"
              [ngTemplateOutletContext]="{ $implicit: tab }"
            ></ng-container>
          </evo-tab-item>
        </evo-tabs-bar>
      </app-component-preview>
      
    </ng-template>
    
    <ng-template #tabContentTmpl let-tab>
      {{ tab.label }}. <br><br> {{ tab.content }}
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsComponent implements OnInit {
  @HostBinding('class.page-wrapper') readonly baseCss: boolean = true;

  tabsSize: EvoUISizes = EvoUISizes.LARGE;
  tabsType: EvoTabsType = EvoTabsType.ROUNDED;
  tabsBarPosition: EvoTabsBarPosition = EvoTabsBarPosition.LEFT;

  tabs1: ITabItem[] = [
    { label: 'Tab 1', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 2', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 3', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 4', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 5', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' }
  ];
  
  tabs2: ITabItem[] = [
    { label: 'Tab 1', active: false, disabled: false, iconName: 'apps', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 2', active: false, disabled: false, iconName: 'atom', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 3', active: false, disabled: false, iconName: 'gear', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 4', active: false, disabled: false, iconName: 'mail', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 5', active: false, disabled: false, iconName: 'send', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' }
  ];

  tabs3: ITabItem[] = [
    { label: 'Tab 1', active: false, disabled: true, iconName: 'apps', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 2', active: true, disabled: false, iconName: 'atom', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 3', active: false, disabled: true, iconName: 'gear', link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' }
  ];

  tabs4: ITabItem[] = [
    { label: 'Tab 1', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 2', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 3', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 4', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 5', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' }
  ];

  tabs5: ITabItem[] = [
    { label: 'Tab 1', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 2', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 3', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 4', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 5', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' }
  ];

  tabs6: ITabItem[] = [
    { label: 'Tab 1', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 2', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 3', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 4', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' },
    { label: 'Tab 5', active: false, disabled: false, iconName: null, link: null, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi fugit impedit nesciunt reiciendis sapiente?' }
  ];
  
  constructor() { }

  ngOnInit(): void {
  }
  
}
