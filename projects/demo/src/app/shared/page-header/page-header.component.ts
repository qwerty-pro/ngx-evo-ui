import { ChangeDetectionStrategy, Component, HostBinding, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-page-header',
  template: `
    <h2 class="evo-heading">{{ pageTitle }}</h2>
    <ng-container *ngIf="descriptionTmpl" [ngTemplateOutlet]="descriptionTmpl"></ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageHeaderComponent {
  @HostBinding('class.page-header') readonly baseCss: boolean = true;
  @Input() pageTitle!: string;
  @Input() descriptionTmpl: TemplateRef<void> | null;

  constructor() {
    this.descriptionTmpl = null;
  }
}
