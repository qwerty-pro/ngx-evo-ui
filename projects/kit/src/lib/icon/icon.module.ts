import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { EvoIconService } from './icon.service';
import { EvoIconStore } from './icon.models';
import { IconComponent } from './icon.component';
import { EVO_ICON_STORE } from './icon.constant';
import icons from './icon-store.const';

const declarations: any[] = [IconComponent];

@NgModule({
  declarations,
  imports: [CommonModule, HttpClientModule],
  exports: [...declarations]
})
export class EvoIconModule {
  static forRoot(additionalIcons: EvoIconStore = new Map()): ModuleWithProviders<EvoIconModule> {
    if (!additionalIcons.size) {
      Array.from(additionalIcons.keys()).forEach((key: string) => {
        if (icons.has(key)) {
          throw `An icon with name ${key} is already exists`
        }
      });
    }
    
    return {
      ngModule: EvoIconModule,
      providers: [
        EvoIconService,
        {
          provide: EVO_ICON_STORE,
          useValue: new Map([...icons, ...additionalIcons])
        }
      ]
    };
  }
}
