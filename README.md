# NgxEvoUI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.5.
This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

# Installation Guide (Library demo application)

1) Clone [repository](https://gitlab.com/qwerty-pro/ngx-evo-ui) from gitlab
2) Come to ***ngx-evo-ui*** directory and execute command **```npm install```** for install all dependencies
3) Execute command **```npm run kit:watch```** for build library (kit) 
4) Execute command **```npm run demo:start```** for start application (demo)
5) Open [demo application](http://localhost:4200) in your browser 
