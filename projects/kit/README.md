# NgxEvoUI

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

# Installation and usage guide

1) Execute command ```npm i ngx-evo-ui``` in root your application
2) Update configuration in ***angular.json***
   ```
    "architect": {
      "build": {
        "options": {
          "assets": {
            {
              "glob": "**/*",
              "input": "./node_modules/ngx-evo-ui/src/assets",
              "output": "./assets/"
            }
          },
          "stylePreprocessorOptions": {
            "includePaths": [
              "node_modules/ngx-evo-ui/styles"
            ]
          }
        }
      }
    }
   ```
3) Connect library styles in ***styles.scss***.
   > In **$theme-config** you need to initialize the themes for each component of the library.
   ```
    @use "lib" as library;
    
    $theme-config: (
      accordion: (),
      heading: ()
    );
    
    @include library.configure($theme: $theme-config);
    @include library.style();
    @include library.add-fonts();
   ```
   
   If you want to override the theme you can pass the required config
   ```
   $theme-config: (
    accordion: ($overrided-accordion-theme),
   );
   ```
4) Import modules in ***app.module.ts***
   ```
   import { EvoAccordionModule } from 'ngx-evo-ui';
   
   @NgModule({
     declarations: [
       AppComponent
     ],
     imports: [
       ...
       EvoAccordionModule
     ]
     bootstrap: [AppComponent]
   })
   export class AppModule { }
   ```
  

# Versions
v7.0.0 - update angular version

v6.0.0 - add checkbox module and radio-group module

v5.0.1 - fix animation import

v5.0.0 - add input module (text-input, radio-input, number-input, textarea)

v4.0.2 - icon module improvement

v4.0.1 - fix main section styles in app-component-preview

v4.0.0 - add button module

v3.0.2 - update icon component

v3.0.1 - create utils service for common functional of components

v3.0.0 - add tabs module

v2.0.5 - update accordion module

v2.0.4 - small update icon module

v2.0.3 - update icon module (separate library icons from applications icons)

v2.0.2 - update package.json and readme.md

v2.0.1 - update package.json

v2.0.0 - add accordion module

v1.0.0 - add icon module with default icons collection

v0.0.0 - initial project
