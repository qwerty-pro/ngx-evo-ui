import { TemplateRef } from '@angular/core';

export enum EvoButtonColors {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
  ERROR = 'error',
  INFO = 'info',
  WARNING = 'warning',
  SUCCESS = 'success',
  GHOST = 'ghost'
}

export enum EvoButtonGroupColors {
  PRIMARY = 'primary',
  ERROR = 'error',
  INFO = 'info',
  WARNING = 'warning',
  SUCCESS = 'success',
}

export enum EvoButtonTypes {
  BUTTON = 'button',
  RESET = 'reset',
  SUBMIT = 'submit'
}

export type EvoButtonGroupItem = {
  label?: string | TemplateRef<any>,
  iconName?: string,
  active: boolean,
  disabled: boolean,
  onClick: (btn: EvoButtonGroupItem) => void
}

export type EvoButtonGroup = EvoButtonGroupItem[];
