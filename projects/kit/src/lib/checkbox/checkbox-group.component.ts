import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EvoCheckboxGroup, EvoCheckboxItem } from './checkbox.models';

@Component({
  selector: 'evo-checkbox-group',
  template: `
    <ng-container *ngIf="checkAllMode">
      <section class="evo-checkbox-group__check-all">
        <evo-checkbox [(model)]="checkedAll" (modelChange)="toggleAll($event)">{{ checkAllLabel }}</evo-checkbox>
      </section>
    </ng-container>
    <section class="evo-checkbox-group__grid" #grid>
      <ng-container *ngFor="let checkbox of model">
        <evo-checkbox #checkbox
          [disabled]="disabledAll"
          [(model)]="checkbox.checked"
          (modelChange)="updateCheckAllSign()">
          {{ checkbox.label }}
        </evo-checkbox>
      </ng-container>
    </section>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxGroupComponent),
      multi: true
    }
  ]
})
export class CheckboxGroupComponent implements ControlValueAccessor, OnInit, AfterViewInit {
  @HostBinding('class.evo-checkbox-group') private readonly baseCss: boolean = true;

  @Input() columns: number = 1;
  @Input() rows: number = 1;
  @Input() checkAllLabel: string = '';
  @Input() checkAllMode: boolean = false;
  @Input() disabledAll: boolean = false;
  @Input()
  set model(value: EvoCheckboxGroup) {
    this._value = [...value];
  }
  
  get model(): EvoCheckboxGroup {
    return this._value;
  }
  
  set checkedAll(value: boolean) {
    this._checkedAll = value;
  }
  
  get checkedAll(): boolean {
    return this._checkedAll;
  }

  @Output() modelChange: EventEmitter<EvoCheckboxGroup> = new EventEmitter<EvoCheckboxGroup>();
  
  @ViewChild('grid', { static: true })
  private readonly _grid: ElementRef<HTMLElement> = new ElementRef<HTMLElement>(this._elRef.nativeElement);
  
  private _checkedAll: boolean = false;
  private _value: EvoCheckboxGroup = [];
  private _onChange: (value: EvoCheckboxGroup) => void = () => {};
  private _onTouched: () => void = () => {};

  constructor(
    private readonly _cdRef: ChangeDetectorRef,
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2
  ){ }
  
  ngOnInit(): void {
    this.setGridTemplate();
  }
  
  ngAfterViewInit(): void {
    this._checkedAll = this._value.every((checkbox: EvoCheckboxItem) => checkbox.checked);
  }
  
  writeValue(value: EvoCheckboxGroup): void {
    this._value = [...value];
  }
  
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabledAll = isDisabled;
  }
  
  updateCheckAllSign(): void {
    this._checkedAll = this._value.every((checkbox: EvoCheckboxItem) => checkbox.checked);
    this.modelChange.emit(this.model);
  }
  
  toggleAll(value: boolean): void {
    this._value.forEach((checkbox: EvoCheckboxItem) => checkbox.checked = value);
    this._checkedAll = value;

    this.modelChange.emit(this.model);
  }

  private setGridTemplate(): void {
    this._renderer.setStyle(this._grid.nativeElement, 'grid-template-columns', `repeat(${this.columns}, 1fr)`);
    this._renderer.setStyle(this._grid.nativeElement, 'grid-template-rows', `repeat(${this.rows}, 1fr)`);
  }
}
