import { ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';
import { EvoAccordionPanel } from './accordion.models';

@Component({
  selector: 'evo-accordion',
  template: `
    <ng-container *ngFor="let panel of panels">
      <evo-accordion-panel [active]="panel.active"
                           [content]="panel.content"
                           [header]="panel.header"
                           [disabled]="panel.disabled"
                           [borderlessMode]="borderLessMode"
                           (panelToggle)="toggle(panel)"
      ></evo-accordion-panel>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccordionComponent {
  @HostBinding('class.evo-accordion') private readonly baseCss: boolean = true;
  
  @Input() accordionMode: boolean = false;
  @Input() borderLessMode: boolean = false;
  @Input() panels: EvoAccordionPanel[] = [];
  
  
  toggle(panel: EvoAccordionPanel) {
    if (this.accordionMode) {
      this.panels.forEach(
        (_panel: EvoAccordionPanel) => _panel.active = Object.is(_panel, panel) ? !_panel.active : false
      );
    } else {
      panel.active = !panel.active;
    }
  }
}
