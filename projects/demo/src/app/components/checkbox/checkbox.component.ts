import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { EvoCheckboxGroup, EvoUISizes, EvoRadioGroup } from '@evo-kit';


enum CheckboxView {
  CHECKBOX = 'checkbox',
  SWITCH = 'switch'
}

@Component({
  selector: 'app-checkbox',
  template: `
    <app-page-header pageTitle="Checkbox"></app-page-header>
    <app-page-content [leftSectionTmpl]="leftSectionTmpl" [rightSectionTmpl]="rightSectionTmpl"></app-page-content>
    
    <ng-template #leftSectionTmpl>
      <app-component-preview name="Checkbox" description="Basic usage of checkbox">
        <evo-checkbox [(model)]="model1" (modelChange)="changeModel1($event)">Simple checkbox</evo-checkbox>
      </app-component-preview>
  
      <app-component-preview name="Disable mode" description="Usage checkbox with disabled mode">
        <div class="column-flex">
          <ng-container *ngFor="let checkbox of disabledCheckboxes">
            <evo-checkbox [disabled]="true"
                          [(model)]="checkbox.checked"
                          (modelChange)="changeModel2($event)">
              {{ checkbox.label }}
            </evo-checkbox>
          </ng-container>
        </div>
      </app-component-preview>
      
      <app-component-preview name="Sizes" description="Checkbox sizes: small, medium (default), large">
        <div class="column-flex">
          <ng-container *ngFor="let checkbox of sizeCheckboxes">
            <evo-checkbox [size]="checkbox.size || defaultSize"
                          [(model)]="checkbox.checked"
                          (modelChange)="changeModel3($event)">
              {{ checkbox.label }}
            </evo-checkbox>
          </ng-container>
        </div>
      </app-component-preview>
      
      <app-component-preview name="Checkbox group" description="Basic usage of checkbox group">
        <evo-checkbox-group [columns]="3" [rows]="2" [(model)]="checkboxGroup1" (modelChange)="changeGroup1($event)"></evo-checkbox-group>
      </app-component-preview>
      
      <app-component-preview name="Reactive form" description="Usage checkbox in the reactive form">
        {{ form.value | json }}
        <br>
        <br>
        <evo-radio-group [(model)]="selectedView" [items]="viewTypes"></evo-radio-group>
        <br>
        <form class="demo-form" [formGroup]="form">
          <ng-container [ngSwitch]="selectedView">
            <ng-container *ngSwitchCase="CheckboxView.CHECKBOX">
              <evo-checkbox formControlName="wifi">Wi-fi</evo-checkbox>
              <evo-checkbox formControlName="bluetooth">Bluetooth</evo-checkbox>
            </ng-container>
            <ng-container *ngSwitchCase="CheckboxView.SWITCH">
              <evo-switch formControlName="wifi">Wi-fi</evo-switch>
              <evo-switch formControlName="bluetooth">Bluetooth</evo-switch>
            </ng-container>
          </ng-container>
        </form>
      </app-component-preview>
    </ng-template>
    
    <ng-template #rightSectionTmpl>
      <app-component-preview name="Switch" description="Basic usage os switch">
        <evo-switch [(model)]="model2">Simple switch</evo-switch>
      </app-component-preview>
  
      <app-component-preview name="Disable mode" description="Usage switch with disabled mode">
        <div class="column-flex">
          <ng-container *ngFor="let switchItem of disabledSwitches">
            <evo-switch [disabled]="true"
                        [(model)]="switchItem.checked"
                        (modelChange)="changeModel2($event)">
              {{ switchItem.label }}
            </evo-switch>
          </ng-container>
        </div>
      </app-component-preview>
  
      <app-component-preview name="Sizes" description="Switch sizes: small, medium (default), large">
        <div class="column-flex">
          <ng-container *ngFor="let switchItem of sizeSwitches">
            <evo-switch [size]="switchItem.size || defaultSize"
                        [(model)]="switchItem.checked"
                        (modelChange)="changeModel3($event)">
              {{ switchItem.label }}
            </evo-switch>
          </ng-container>
        </div>
      </app-component-preview>
  
      <app-component-preview name="Check all" description="Usage checkbox group with check all mode">
        <evo-checkbox-group
          checkAllLabel="Check all"
          [checkAllMode]="true"
          [columns]="3"
          [rows]="2"
          [(model)]="checkboxGroup2"
          (modelChange)="changeGroup2($event)"
        ></evo-checkbox-group>
      </app-component-preview>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent implements OnInit {
  @HostBinding('class.page-wrapper') private readonly baseCss: boolean = true;

  defaultSize: EvoUISizes = EvoUISizes.MEDIUM;

  model1: boolean = false;
  model2: boolean = false;

  disabledCheckboxes: EvoCheckboxGroup = [
    { checked: false, label: 'Checkbox 1' },
    { checked: true, label: 'Checkbox 2' }
  ];
  
  disabledSwitches: EvoCheckboxGroup = [
    { checked: false, label: 'Switch 1' },
    { checked: true, label: 'Switch 2' }
  ];

  sizeCheckboxes: EvoCheckboxGroup = [
    { checked: false, label: 'Small checkbox', size: EvoUISizes.SMALL },
    { checked: false, label: 'Medium checkbox', size: EvoUISizes.MEDIUM },
    { checked: false, label: 'Large checkbox', size: EvoUISizes.LARGE },
  ];

  sizeSwitches: EvoCheckboxGroup = [
    { checked: false, label: 'Small switch', size: EvoUISizes.SMALL },
    { checked: false, label: 'Medium switch', size: EvoUISizes.MEDIUM },
    { checked: false, label: 'Large switch', size: EvoUISizes.LARGE },
  ];

  checkboxGroup1: EvoCheckboxGroup = [
    { checked: false, label: 'A' },
    { checked: false, label: 'B' },
    { checked: false, label: 'C' },
    { checked: false, label: 'D' },
    { checked: false, label: 'E' },
    { checked: false, label: 'F' },
  ];

  checkboxGroup2: EvoCheckboxGroup = [
    { checked: false, label: 'A' },
    { checked: false, label: 'B' },
    { checked: false, label: 'C' },
    { checked: false, label: 'D' },
    { checked: false, label: 'E' },
    { checked: false, label: 'F' },
  ];

  viewTypes: EvoRadioGroup = [
    { label: 'Checkbox Mode', value: CheckboxView.CHECKBOX },
    { label: 'Switch Mode', value: CheckboxView.SWITCH },
  ];

  form: UntypedFormGroup;

  selectedView: string = CheckboxView.SWITCH;

  readonly CheckboxView: typeof CheckboxView = CheckboxView;

  constructor(private readonly _fb: UntypedFormBuilder) {
    this.form = this._fb.group({
      wifi: new UntypedFormControl(false),
      bluetooth: new UntypedFormControl(false)
    })
  }

  ngOnInit(): void {
  }

  changeModel1(value: boolean) {
    console.log(value);
  }

  changeModel2(value: boolean) {
    console.log(value);
  }

  changeModel3(value: boolean) {
    console.log(value);
  }
  
  changeGroup1(value: EvoCheckboxGroup) {
    console.log(value);
  }
  
  changeGroup2(value: EvoCheckboxGroup) {
    console.log(value);
  }
}
