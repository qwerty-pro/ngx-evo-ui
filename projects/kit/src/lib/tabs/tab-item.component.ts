import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'evo-tab-item',
  template: `
    <div class="evo-tabs-bar__content-wrapper" [hidden]="!active">
      <ng-content></ng-content>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabItemComponent {
  @Input() active: boolean = false;
  @Input() disabled: boolean = false;
  @Input() label: string = '';
  @Input() link: string | any[] = '';
  @Input() iconName: string | null = null;
  
  constructor(public readonly _cdRef: ChangeDetectorRef){ }
}
