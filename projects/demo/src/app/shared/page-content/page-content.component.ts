import { ChangeDetectionStrategy, Component, HostBinding, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-page-content',
  template: `
    <ng-container *ngIf="mainSectionTmpl">
      <section class="column-grid">
        <ng-container [ngTemplateOutlet]="mainSectionTmpl"></ng-container>
      </section>
    </ng-container>
    <ng-container *ngIf="!mainSectionTmpl">
      <section class="section">
        <ng-container [ngTemplateOutlet]="leftSectionTmpl"></ng-container>
      </section>
      <section class="section">
        <ng-container [ngTemplateOutlet]="rightSectionTmpl"></ng-container>
      </section>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageContentComponent {
  @HostBinding('class.page-content') readonly baseCss: boolean = true;

  @Input() mainSectionTmpl: TemplateRef<void> | null = null;
  @Input() leftSectionTmpl: TemplateRef<void> | null = null;
  @Input() rightSectionTmpl: TemplateRef<void> | null = null;
}
