export enum EvoUISizes {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}

export enum DirectionTypes {
  COLUMN = 'column',
  ROW = 'row'
}

export interface EvoOptionItem {
  label: string;
  value: string | number | boolean;
}
