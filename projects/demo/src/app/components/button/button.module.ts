import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoButtonModule } from '@evo-kit';
import { SharedModule } from '../../shared';

import { ButtonRoutingModule } from './button-routing.module';
import { ButtonComponent } from './button.component';



@NgModule({
  declarations: [
    ButtonComponent
  ],
  imports: [
    ButtonRoutingModule,
    CommonModule,
    EvoButtonModule,
    SharedModule
  ]
})
export class ButtonModule { }
