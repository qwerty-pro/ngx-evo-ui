import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { EvoButtonColors, EvoButtonTypes } from './button.models';
import { EvoUISizes, UtilsService } from '../@utils';

@Component({
  selector: 'evo-button',
  template: `
    <button class="evo-button__button"
            [disabled]="disabled"
            [type]="type"
            (click)="onButtonClick()">
      <ng-content></ng-content>
    </button>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @HostBinding('class.evo-button') private readonly baseCss: boolean = true;
  
  @Input() color: EvoButtonColors = EvoButtonColors.PRIMARY;
  @Input() disabled: boolean = false;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input() type: EvoButtonTypes = EvoButtonTypes.BUTTON;
  
  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();
  
  @HostBinding('class') get additionalClasses() {
    return `
      evo-button-${this._utilsService.getButtonColorClass(this.color)}
      evo-button-${this._utilsService.getSizeClass(this.size)}
    `;
  }
  
  constructor(private readonly _utilsService: UtilsService) { }
  
  onButtonClick(): void {
    this.onClick.emit();
  }
}
