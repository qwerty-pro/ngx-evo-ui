import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EvoIconModule } from '../icon';

import { CheckboxComponent } from './checkbox.component';
import { CheckboxGroupComponent } from './checkbox-group.component';
import { SwitchComponent } from './switch.component';

const declarations: any[] = [
  CheckboxComponent,
  CheckboxGroupComponent,
  SwitchComponent
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    EvoIconModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [...declarations]
})
export class EvoCheckboxModule { }
