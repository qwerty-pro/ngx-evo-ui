import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EvoCheckboxModule, EvoRadioGroupModule } from '@evo-kit';
import { SharedModule } from '../../shared';

import { CheckboxComponent } from './checkbox.component';
import { CheckboxRoutingModule } from './checkbox-routing.module';


@NgModule({
  declarations: [
    CheckboxComponent
  ],
  imports: [
    CheckboxRoutingModule,
    CommonModule,
    EvoCheckboxModule,
    EvoRadioGroupModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CheckboxModule { }
