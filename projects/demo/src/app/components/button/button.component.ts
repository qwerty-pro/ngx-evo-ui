import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { EvoButtonColors, EvoButtonGroupColors, EvoButtonGroup, EvoButtonGroupItem, EvoUISizes } from '@evo-kit';

type IButton = { color: EvoButtonColors, disabled: boolean, label: string, size: EvoUISizes };

@Component({
  selector: 'app-button',
  template: `
    <app-page-header pageTitle="Button"></app-page-header>
    <app-page-content [leftSectionTmpl]="leftSectionTmpl" [rightSectionTmpl]="rightSectionTmpl"></app-page-content>
    
    <ng-template #leftSectionTmpl>
      <app-component-preview name="Basic" description="Basic usage of button">
        <evo-button (onClick)="callback1()">Click me!</evo-button>
      </app-component-preview>

      <app-component-preview name="Sizes" description="Button sizes: small, medium (default), large">
        <div class="inline-flex">
          <ng-container *ngFor="let button of buttons1">
            <evo-button [size]="button.size" (onClick)="callback2(button)">{{ button.label }}</evo-button>
          </ng-container>
        </div>
      </app-component-preview>

      <app-component-preview name="Disabled mode" description="Usage with disabled mode">
        <evo-button [disabled]="true" (onClick)="callback1()">Click me!</evo-button>
      </app-component-preview>

      <app-component-preview
        name="Colors"
        description="Button colors: primary (default), secondary, tertiary, ghost, info, error, success, warning">
        <div class="grid-template">
          <ng-container *ngFor="let button of buttons2">
            <evo-button [color]="button.color" (onClick)="callback2(button)">{{ button.label }}</evo-button>
          </ng-container>
        </div>
      </app-component-preview>
    </ng-template>
    
    <ng-template #rightSectionTmpl>
      <app-component-preview name="Basic button group" description="Basic usage of button group">
        <evo-button-group [buttons]="groupButtons1"></evo-button-group>
      </app-component-preview>
      
      <app-component-preview name="Button group sizes" description="Button group sizes: small, medium (default), large">
        <div class="column-flex">
          <ng-container *ngFor="let group of groups1">
            <evo-button-group [buttons]="group.buttons" [size]="group.size"></evo-button-group>
          </ng-container>
        </div>
      </app-component-preview>
      
      <app-component-preview name="Button group with disabled mode" description="Usage with disabled mode">
        <evo-button-group [buttons]="disabledGroupButtons"></evo-button-group>
      </app-component-preview>
      
      <app-component-preview
        name="Button group colors"
        description="Button group colors: primary (default), info, error, success, warning">
        <div class="column-flex">
          <ng-container *ngFor="let group of groups2">
            <evo-button-group [buttons]="group.buttons" [color]="group.color"></evo-button-group>
          </ng-container>
        </div>
      </app-component-preview>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @HostBinding('class.page-wrapper') private readonly baseCss: boolean = true;
  
  buttons1: IButton[] = [
    { color: EvoButtonColors.PRIMARY, disabled: false, label: 'Button #1', size: EvoUISizes.SMALL },
    { color: EvoButtonColors.PRIMARY, disabled: false, label: 'Button #2', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.PRIMARY, disabled: false, label: 'Button #3', size: EvoUISizes.LARGE }
  ];
  
  buttons2: IButton[] = [
    { color: EvoButtonColors.PRIMARY, disabled: false, label: 'Primary', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.SECONDARY, disabled: false, label: 'Secondary', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.TERTIARY, disabled: false, label: 'Tertiary', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.GHOST, disabled: false, label: 'Ghost', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.ERROR, disabled: false, label: 'Error', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.INFO, disabled: false, label: 'Info', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.SUCCESS, disabled: false, label: 'Success', size: EvoUISizes.MEDIUM },
    { color: EvoButtonColors.WARNING, disabled: false, label: 'Warning', size: EvoUISizes.MEDIUM },
  ];
  
  groupButtons1: EvoButtonGroup = [
    { label: 'Button #1', active: true, disabled: false, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: false, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: false, disabled: false, onClick: this.groupCallback3.bind(this) }
  ];

  groupButtons2: EvoButtonGroup = [
    { label: 'Button #1', active: true, disabled: false, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: false, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: false, disabled: false, onClick: this.groupCallback3.bind(this) }
  ];

  groupButtons3: EvoButtonGroup = [
    { label: 'Button #1', active: true, disabled: false, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: false, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: false, disabled: false, onClick: this.groupCallback3.bind(this) }
  ];

  groupButtons4: EvoButtonGroup = [
    { label: 'Button #1', active: true, disabled: false, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: false, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: false, disabled: false, onClick: this.groupCallback3.bind(this) }
  ];

  groupButtons5: EvoButtonGroup = [
    { label: 'Button #1', active: true, disabled: false, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: false, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: false, disabled: false, onClick: this.groupCallback3.bind(this) }
  ];

  disabledGroupButtons: EvoButtonGroup = [
    { label: 'Button #1', active: true, disabled: true, onClick: this.groupCallback1.bind(this) },
    { label: 'Button #2', active: false, disabled: true, onClick: this.groupCallback2.bind(this) },
    { label: 'Button #3', active: false, disabled: false, onClick: this.groupCallback3.bind(this) }
  ];

  groups1: { buttons: EvoButtonGroup, size: EvoUISizes }[] = [
    { buttons: [...this.groupButtons1], size: EvoUISizes.SMALL },
    { buttons: [...this.groupButtons2], size: EvoUISizes.MEDIUM },
    { buttons: [...this.groupButtons3], size: EvoUISizes.LARGE },
  ];

  groups2: { buttons: EvoButtonGroup, color: EvoButtonGroupColors }[] = [
    { buttons: [...this.groupButtons1], color: EvoButtonGroupColors.PRIMARY },
    { buttons: [...this.groupButtons2], color: EvoButtonGroupColors.INFO },
    { buttons: [...this.groupButtons3], color: EvoButtonGroupColors.SUCCESS },
    { buttons: [...this.groupButtons4], color: EvoButtonGroupColors.WARNING },
    { buttons: [...this.groupButtons5], color: EvoButtonGroupColors.ERROR }
  ];
  
  
  callback1(): void {
    alert('You successfully clicked on the button');
  }
  
  callback2(button: IButton): void {
    alert(`${button.label} successfully clicked!`);
  }
  
  groupCallback1(button: EvoButtonGroupItem): void {
    console.log('Click from group button 1', button);
  }
  
  groupCallback2(button: EvoButtonGroupItem): void {
    console.log('Click from group button 2', button);
  }
  
  groupCallback3(button: EvoButtonGroupItem): void {
    console.log('Click from group button 3', button);
  }
}
