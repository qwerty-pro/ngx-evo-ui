import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DirectionTypes, EvoUISizes, UtilsService } from '../@utils';
import { EvoRadioGroup } from './radio-group.models';

@Component({
  selector: 'evo-radio-group',
  template: `
    <ng-container *ngFor="let item of items; index as idx">
      <div class="evo-radio-button">
        <input class="evo-radio-button__input" id="{{ radioGroupId }}-{{ idx }}" type="radio"
               [name]="name"
               [ngModel]="model"
               (ngModelChange)="onChangeValue($event)"
               [disabled]="item?.disabled || false"
               [value]="item.value"
        >
        <label class="evo-radio-button__label" for="{{ radioGroupId }}-{{ idx }}">
          {{ item.label }}
        </label>
      </div>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true
    }
  ]
})
export class RadioGroupComponent implements ControlValueAccessor {
  @HostBinding('class.evo-radio-group') private readonly baseCss: boolean = true;

  @Input() name!: string;
  @Input() direction: DirectionTypes = DirectionTypes.ROW;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input() items: EvoRadioGroup = [];
  @Input()
  set model(value: string) {
    this._value = value;
  }
  
  get model(): string {
    return this._value;
  }
  
  @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
  @HostBinding('class') get additionalClasses(): string {
    return `evo-radio-group-${this._utilsService.getSizeClass(this.size)} evo-radio-group__${this.direction}`;
  }

  private _value: string = '';
  private _onChange: (value: string) => void = () => {};
  private _onTouched: () => void = () => {};

  public readonly radioGroupId: string;
  
  constructor(private readonly _utilsService: UtilsService) {
    this.radioGroupId = this._utilsService.generateUniqId();
  }
  
  writeValue(value: string): void {
    this._value = value;
  }
  
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  
  onChangeValue(value: string): void {
    this._value = value;
    
    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
