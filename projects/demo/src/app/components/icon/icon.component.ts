import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { EvoIconService } from '@evo-kit';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent {
  @HostBinding('class.page-wrapper') readonly baseCss: boolean = true;

  public readonly iconNames: string[];

  constructor(private readonly iconService: EvoIconService) {
    this.iconNames = Array.from(this.iconService.iconStore.keys());
  }
}
