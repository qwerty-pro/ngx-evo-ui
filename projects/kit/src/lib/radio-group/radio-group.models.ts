export type EvoRadioButton = { disabled?: boolean, label: string, value: string };
export type EvoRadioGroup = EvoRadioButton[];
