import { EvoUISizes } from '../@utils';

export type EvoCheckboxItem = { checked: boolean, disabled?: boolean, label: string, size?: EvoUISizes };
export type EvoCheckboxGroup = EvoCheckboxItem[];
