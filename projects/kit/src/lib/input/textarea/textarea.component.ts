import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EvoUISizes, UtilsService } from '../../@utils';

@Component({
  selector: 'evo-textarea',
  template: `
    <textarea #textarea class="evo-textarea__input" evoTextareaAutosize
              [disabled]="disabled"
              [minRows]="minRows"
              [maxRows]="maxRows"
              [placeholder]="placeholder"
              [readOnly]="readonly"
              [required]="required"
              [(ngModel)]="model"
              (blur)="onBlur()"
    ></textarea>
    <ng-container *ngIf="maxLength">
      <div class="evo-textarea__characters">
        {{ textarea.value.length }} / {{ maxLength }}
      </div>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    }
  ]
})
export class TextareaComponent implements ControlValueAccessor, OnInit {
  @HostBinding('class.evo-textarea') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() maxLength: string | null = null;
  @Input() minRows: number = 2;
  @Input() maxRows: number = 5;
  @Input() placeholder: string = '';
  @Input() readonly: boolean = false;
  @Input() required: boolean = false;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input()
  set model(value: string) {
    this._value = value;
  }

  get model(): string {
    return this._value;
  }

  @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('textarea', { static: true })
  private _textarea: ElementRef<HTMLTextAreaElement> = new ElementRef<HTMLTextAreaElement>(this._elRef.nativeElement);

  @HostBinding('class') get additionalClasses() {
    return `evo-input-${this._utilsService.getSizeClass(this.size)}`;
  }

  private _value: string = '';
  private _onChange: (value: string) => void = () => {};
  private _onTouched: () => void = () => {};

  constructor(
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    if (this.maxLength) {
      this._renderer.setAttribute(this._textarea.nativeElement, 'maxlength', this.maxLength);
    }
  }

  writeValue(value: string): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onBlur(): void  {
    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
