import { Component, OnInit, ChangeDetectionStrategy, Input, HostBinding } from '@angular/core';
import { IMenuItem } from '../common.interface';

@Component({
  selector: 'app-left-nav',
  template: `
    <ul class="menu">
      <li class="menu-item" *ngFor="let item of menuItems">
        <a [routerLink]="item.link" routerLinkActive="active">{{ item.name }}</a>
      </li>
    </ul>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeftNavComponent implements OnInit {
  @HostBinding('class.left-nav') readonly baseCss: boolean = true;
  @Input() menuItems: IMenuItem[] = [];

  constructor() {}

  ngOnInit(): void {}
}
