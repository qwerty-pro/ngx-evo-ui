import { TemplateRef } from '@angular/core';

export type EvoAccordionPanel = {
  active: boolean,
  content: string | TemplateRef<any>,
  disabled: boolean,
  header: string | TemplateRef<any>
};
