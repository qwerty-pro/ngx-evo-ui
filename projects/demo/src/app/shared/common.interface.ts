export enum LinkEnum {
  ANGULAR = 'angular',
  GITLAB = 'gitlab'
}


export type IMenuItem = { name: string; link: string };
export type ILinkItem = { iconName: string; source: LinkEnum };
export type IComponentPreview = { name: string; description: string; };

