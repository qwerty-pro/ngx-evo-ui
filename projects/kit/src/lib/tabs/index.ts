export * from './tabs.models';
export * from './tabs.module';
export * from './tab-item.component';
export * from './tabs-bar.component';
