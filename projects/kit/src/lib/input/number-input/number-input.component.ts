import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EvoUISizes, UtilsService } from '../../@utils';
import { filter, fromEvent, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'evo-number-input',
  template: `
    <input #numberInput class="evo-number-input__input" type="number"
           [(ngModel)]="model"
           [disabled]="disabled"
           [placeholder]="placeholder"
           [readOnly]="readonly"
           [required]="required"
           (blur)="onBlur()"
    >
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NumberInputComponent),
      multi: true
    }
  ]
})
export class NumberInputComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @HostBinding('class.evo-number-input') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() min: string | null = null;
  @Input() max: string | null = null;
  @Input() placeholder: string = '';
  @Input() readonly: boolean = false;
  @Input() required: boolean = false;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input() step: string | null = null;
  
  @Input()
  set model(value: number | null) {
    this._value = value;
  }
  
  get model(): number | null {
    return this._value;
  }
  
  @Output() modelChange: EventEmitter<number | null> = new EventEmitter<number | null>();
  @ViewChild('numberInput', { static: true })
  private _numberInput: ElementRef<HTMLInputElement> = new ElementRef<HTMLInputElement>(this._elRef.nativeElement);
  
  @HostBinding('class') get additionalClasses(): string {
    return `evo-input-${this._utilsService.getSizeClass(this.size)}`;
  }

  private readonly _SPECIAL_SYMBOLS_CODES: string[] = ['KeyE', 'Minus', 'Equal', 'Period'];
  private _subscription: Subscription = Subscription.EMPTY;
  private _value: number | null = null;
  private _onChange: (value: number | null) => void = () => {};
  private _onTouched: () => void = () => {};
  
  constructor(
    private readonly _elRef: ElementRef,
    private readonly _renderer: Renderer2,
    private readonly _utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.setNumberInputAttributes();
  
    this._subscription = fromEvent<KeyboardEvent>(this._numberInput.nativeElement, 'keypress')
      .pipe(
        tap((event) => {
          if (this._SPECIAL_SYMBOLS_CODES.includes(event.code)) {
            event.preventDefault();
          
            return;
          }
        })
      )
      .subscribe();
  }
  
  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
  
  writeValue(value: number | null): void {
    this._value = value;
  }
  
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  
  onBlur(): void {
    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
  
  private setNumberInputAttributes(): void {
    if (this.min) {
      this._renderer.setAttribute(this._numberInput.nativeElement, 'min', this.min);
    }
    
    if (this.max) {
      this._renderer.setAttribute(this._numberInput.nativeElement, 'max', this.max)
    }
    
    if (this.step) {
      this._renderer.setAttribute(this._numberInput.nativeElement, 'step', this.step);
    }
  }
}
