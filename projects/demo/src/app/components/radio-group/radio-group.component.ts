import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { EvoRadioGroup, EvoUISizes } from '@evo-kit';


@Component({
  selector: 'app-radio-group',
  template: `
    <app-page-header pageTitle="Radio group"></app-page-header>
    <app-page-content [leftSectionTmpl]="leftSectionTmpl" [rightSectionTmpl]="rightSectionTmpl"></app-page-content>
    
    <ng-template #leftSectionTmpl>
      <app-component-preview name="Radio group" description="Usage radio group">
        <evo-radio-group name="radioGroup1"
                         [items]="radioGroup1"
                         [(model)]="radioModel1"
                         (modelChange)="changeRadio1($event)"
        ></evo-radio-group>
      </app-component-preview>

      <app-component-preview name="Radio input sizes" description="Sizes: small, medium (default), large">
        <div class="column-flex">
          <ng-container *ngFor="let radioSize of radioSizes; let i = index">
            <evo-radio-group [name]="'radioGroup_' + i"
                             [size]="radioSize.size"
                             [items]="radioSize.collection"
                             [(model)]="radioSize.model"
                             (modelChange)="changeRadio3($event)"
            ></evo-radio-group>
          </ng-container>
        </div>
      </app-component-preview>
    </ng-template>
    
    <ng-template #rightSectionTmpl>
      <app-component-preview name="Disabled radio button" description="Radio group with disabled radio button">
        <evo-radio-group name="radioGroup2"
                         [items]="radioGroup2"
                         [(model)]="radioModel2"
                         (modelChange)="changeRadio2($event)"
        ></evo-radio-group>
      </app-component-preview>

      <app-component-preview name="Reactive form" description="Usage components in reactive forms">
        {{ form.value | json }}
        <br>
        <br>
        <form class="demo-form" [formGroup]="form">
          <evo-radio-group formControlName="gender" [items]="genderTypes"></evo-radio-group>
        </form>
      </app-component-preview>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadioGroupComponent implements OnInit {
  @HostBinding('class.page-wrapper') private readonly baseCss: boolean = true;

  radioModel1: string = 'Value 1';
  radioModel2: string = 'Value 2';

  radioGroup1: EvoRadioGroup = [
    { disabled: false, label: 'Radio 1', value: 'Value 1' },
    { disabled: false, label: 'Radio 2', value: 'Value 2' },
    { disabled: false, label: 'Radio 3', value: 'Value 3' }
  ];

  radioGroup2: EvoRadioGroup = [
    { disabled: true, label: 'Radio 1', value: 'Value 1' },
    { disabled: true, label: 'Radio 2', value: 'Value 2' }
  ];

  radioGroup3: EvoRadioGroup = [
    { disabled: false, label: 'Radio 1', value: 'Value 1' },
    { disabled: false, label: 'Radio 2', value: 'Value 2' }
  ];

  radioGroup4: EvoRadioGroup = [
    { disabled: false, label: 'Radio 1', value: 'Value 1' },
    { disabled: false, label: 'Radio 2', value: 'Value 2' }
  ];

  radioGroup5: EvoRadioGroup = [
    { disabled: false, label: 'Radio 1', value: 'Value 1' },
    { disabled: false, label: 'Radio 2', value: 'Value 2' }
  ];

  genderTypes: EvoRadioGroup = [
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' },
  ];

  radioSizes: { collection: EvoRadioGroup, model: string, size: EvoUISizes }[] = [
    { collection: this.radioGroup3, model: '', size: EvoUISizes.SMALL },
    { collection: this.radioGroup4, model: '', size: EvoUISizes.MEDIUM },
    { collection: this.radioGroup5, model: '', size: EvoUISizes.LARGE },
  ];

  form: UntypedFormGroup;

  constructor(private readonly _fb: UntypedFormBuilder) {
    this.form = this._fb.group({
      gender: new UntypedFormControl('')
    });
  }

  ngOnInit(): void {
  }

  changeRadio1(value: string): void {
    console.log(this.radioModel1, value);
  }

  changeRadio2(value: string): void {
    console.log(this.radioModel2, value);
  }

  changeRadio3(value: string): void {
    console.log(value);
  }
}
