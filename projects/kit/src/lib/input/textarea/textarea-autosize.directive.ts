import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[evoTextareaAutosize]'
})
export class TextareaAutosizeDirective implements OnInit {
  @Input() minRows: number = 2;
  @Input() maxRows: number = 5;

  private readonly _avgLineHeight: number = 20;
  private _offsetHeight: number = 0;

  constructor(private readonly _elRef: ElementRef<HTMLTextAreaElement>) { }
  
  @HostListener('input')
  onInput(): void {
    this.resize();
  }

  ngOnInit(): void {
    if (this._elRef.nativeElement.scrollHeight)  {
      setTimeout(() => this.resize(), 0);
    }
  }

  private resize(): void {
    if (this._offsetHeight <= 0) {
      this._offsetHeight = this._elRef.nativeElement.scrollHeight;
    }

    this._elRef.nativeElement.rows = this.minRows;

    const rows: number = Math.floor(
      (this._elRef.nativeElement.scrollHeight - this._offsetHeight) / this._avgLineHeight
    );
    const rowsCount: number = this.minRows + rows;

    this._elRef.nativeElement.rows = rowsCount > this.maxRows ? this.maxRows : rowsCount;
  }
}
