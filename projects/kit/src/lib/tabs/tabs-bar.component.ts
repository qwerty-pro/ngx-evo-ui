import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  QueryList,
  ViewEncapsulation
} from '@angular/core';
import { EvoTabsBarPosition, EvoTabsType } from './tabs.models';
import { EvoUISizes, UtilsService } from '../@utils';
import { TabItemComponent } from './tab-item.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'evo-tabs-bar',
  template: `
    <ul class="evo-tabs-bar__tabs">
      <li class="evo-tab-item" [style.width.px]="optimalTabWidth" *ngFor="let tab of tabs">
        <ng-container
          [ngTemplateOutlet]="linkMode ? routeTabTmpl : tabTmpl"
          [ngTemplateOutletContext]="{ $implicit: tab }">
        </ng-container>
      </li>
    </ul>
    <ng-content select="evo-tab-item"></ng-content>

    <ng-template #tabTmpl let-tab>
      <button class="evo-tab-item__action"
              [class.active]="tab.active"
              [disabled]="tab.disabled"
              [title]="tab.label"
              (click)="selectTab(tab)" >
        <evo-icon class="evo-tab-item__icon" [name]="tab.iconName" *ngIf="tab.iconName"></evo-icon>
        <span class="evo-tab-item__text">{{ tab.label }}</span>
      </button>
    </ng-template>

    <ng-template #routeTabTmpl let-tab>
      <button class="evo-tab-item__action"
              routerLinkActive="active"
              [disabled]="tab.disabled"
              [title]="tab.label">
        <evo-icon class="evo-tab-item__icon" [name]="tab.iconName" *ngIf="tab.iconName"></evo-icon>
        <a class="evo-tab-item_text" [routerLink]="tab.link">{{ tab.label }}</a>
      </button>
    </ng-template>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsBarComponent implements AfterContentInit, AfterViewInit, OnDestroy {
  @HostBinding('class.evo-tabs-bar') private readonly baseCss: boolean = true;

  @Input() linkMode: boolean = false;
  @Input() position: EvoTabsBarPosition = EvoTabsBarPosition.TOP;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input() type: EvoTabsType = EvoTabsType.BLOCK;

  @ContentChildren(TabItemComponent) tabs: QueryList<TabItemComponent> = new QueryList<TabItemComponent>();

  @HostBinding('class') get additionalClasses() {
    return `evo-tabs-bar-${this.position} ${this.type}-tabs tabs-size__${this._utilsService.getSizeClass(this.size)}`;
  }

  private readonly MAX_TAB_WIDTH: number = 150;
  private readonly TAB_OFFSET_VALUE: number = 2;

  optimalTabWidth: number = 0;
  subscription: Subscription = Subscription.EMPTY;

  constructor(
    private readonly _elRef: ElementRef<HTMLElement>,
    private readonly _cdRef: ChangeDetectorRef,
    private readonly _utilsService: UtilsService
  ) {}

  ngAfterContentInit(): void {
    if (this.linkMode) {
      this.checkLinkInTabs();
    }

    this.subscription = this.tabs.changes.subscribe((tabs: TabItemComponent[]) => {
      const activeTab: TabItemComponent | undefined = tabs.find((tab: TabItemComponent) => tab.active);
      this.selectTab(activeTab || this.tabs.first);
    });

    this.tabs.notifyOnChanges();
  }

  ngAfterViewInit(): void {
    this.optimizeTabWidth();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  selectTab(tab: TabItemComponent): void {
    this.tabs.forEach((_tab: TabItemComponent) => {
      _tab.active = Object.is(_tab, tab);
      _tab._cdRef.detectChanges();
    });
  }

  private optimizeTabWidth(): void {
    const componentWidth: number = this._elRef.nativeElement.offsetWidth;
    const optimalTabWidth = Math.floor(componentWidth / this.tabs.length);
    this.optimalTabWidth = optimalTabWidth > this.MAX_TAB_WIDTH
      ? this.MAX_TAB_WIDTH
      : optimalTabWidth - this.TAB_OFFSET_VALUE;

    this._cdRef.detectChanges();
  }

  private checkLinkInTabs(): void {
    let invalidRouteTab: TabItemComponent | undefined = this.tabs.find((tab: TabItemComponent) => !tab.link);

    if (invalidRouteTab) {
      throw `Error! Can't read property 'link' in the tab with label ${invalidRouteTab.label}`;
    }
  }
}
