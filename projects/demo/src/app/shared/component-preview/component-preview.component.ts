import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'app-component-preview',
  template: `
    <h4 class="evo-heading">{{ name }}</h4>
    <p class="component-preview__description">{{ description }}</p>
    <div class="component-preview__preview">
      <ng-content></ng-content>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentPreviewComponent {
  @HostBinding('class.component-preview') readonly baseCss: boolean = true;
  @Input() name: string = '';
  @Input() description: string = '';
}
