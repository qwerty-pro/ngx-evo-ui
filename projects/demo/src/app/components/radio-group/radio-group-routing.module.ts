import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioGroupComponent } from './radio-group.component';

const routes: Routes = [
  { path: '', component: RadioGroupComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RadioGroupRoutingModule {}
