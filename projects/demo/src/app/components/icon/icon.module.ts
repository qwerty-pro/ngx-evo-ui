import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoIconModule } from '@evo-kit';

import { IconComponent } from './icon.component';
import { IconRoutingModule } from './icon-routing.module';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [IconComponent],
  imports: [CommonModule, EvoIconModule, IconRoutingModule, SharedModule]
})
export class IconModule {}
