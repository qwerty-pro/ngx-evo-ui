import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EvoUISizes, UtilsService } from '../@utils';

@Component({
  selector: 'evo-checkbox',
  template: `
    <evo-icon class="evo-checkbox__input_checked" name="check" *ngIf="model"
              [class.evo-checkbox__input_disabled]="disabled"
              (click)="onChange(false)"
    ></evo-icon>
    <input class="evo-checkbox__input" type="checkbox"
           [disabled]="disabled"
           [id]="checkboxId"
           [(ngModel)]="model"
           (ngModelChange)="onChange($event)"
    >
    <label class="evo-checkbox__label" for="{{ checkboxId }}">
      <ng-content></ng-content>
    </label>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent implements ControlValueAccessor {
  @HostBinding('class.evo-checkbox') private readonly baseCss: boolean = true;

  @Input() disabled: boolean = false;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input()
  set model(value: boolean) {
    this._value = value;
  }

  get model(): boolean {
    return this._value;
  }

  get checkboxId(): string {
    return this._checkboxId;
  }

  @Output() modelChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @HostBinding('class') get additionalClasses(): string {
    return `evo-checkbox-${this._utilsService.getSizeClass(this.size)}`;
  }

  private readonly _checkboxId: string;
  private _value: boolean = false;
  private _onChange: (value: boolean) => void = () => {};
  private _onTouched: () => void = () => {};

  constructor(
    public readonly _cdRef: ChangeDetectorRef,
    private readonly _utilsService: UtilsService
  ) {
    this._checkboxId = this._utilsService.generateUniqId();
  }

  writeValue(value: boolean): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: boolean): void {
    this._value = value;

    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
