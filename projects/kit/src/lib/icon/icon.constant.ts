import { InjectionToken } from '@angular/core';
import { EvoIconStore } from './icon.models';

export const EVO_ICON_STORE: InjectionToken<EvoIconStore> = new InjectionToken<EvoIconStore>('EVO_ICON_STORE');
