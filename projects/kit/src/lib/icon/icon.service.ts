import { Inject, Injectable, SecurityContext } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { finalize, map, Observable, of, share } from 'rxjs';
import { EvoIcon, EvoIconStore } from './icon.models';
import { EVO_ICON_STORE } from './icon.constant';

@Injectable({
  providedIn: 'root'
})
export class EvoIconService {
  private readonly icons: EvoIconStore;
  private readonly inProgressUrlFetches: Map<string, Observable<string>> = new Map<string, Observable<string>>();

  get iconStore(): EvoIconStore {
    return this.icons;
  }
  
  constructor(
    @Inject(EVO_ICON_STORE) icons: EvoIconStore,
    private readonly http: HttpClient,
    private readonly sanitizer: DomSanitizer
  ) {
    this.icons = icons;
  }

  getIconByName(name: string): Observable<string> {
    const foundIcon: EvoIcon | undefined = this.icons.get(name);

    if (!foundIcon) {
      throw `Icon with name "${name}" not found`;
    }

    const inProgressFetch: Observable<string> | undefined = this.inProgressUrlFetches.get(foundIcon.url);

    if (inProgressFetch) {
      return inProgressFetch;
    }

    if (Boolean(foundIcon.data)) {
      return of(foundIcon.data as string);
    }

    const fetchIcon$: Observable<string> = this.downloadIcon(name, foundIcon);
    this.inProgressUrlFetches.set(foundIcon.url, fetchIcon$);

    return fetchIcon$;
  }

  private downloadIcon(name: string, icon: EvoIcon): Observable<string> {
    const resource: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(icon.url);
    const url: string = this.sanitizer.sanitize(SecurityContext.URL, resource) as string;

    return this.http.get(url, { responseType: 'text' }).pipe(
      finalize(() => this.inProgressUrlFetches.delete(icon.url)),
      share(),
      map((data: string) => {
        const loadedIcon: EvoIcon = { ...icon, data };
        this.icons.set(name, loadedIcon);

        return data;
      })
    );
  }
}
