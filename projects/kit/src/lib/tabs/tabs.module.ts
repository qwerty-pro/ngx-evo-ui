import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EvoIconModule } from '../icon';

import { TabsBarComponent } from './tabs-bar.component';
import { TabItemComponent } from './tab-item.component';


const declarations: any[] = [
  TabsBarComponent,
  TabItemComponent
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    EvoIconModule,
    RouterModule
  ],
  exports: [...declarations]
})
export class EvoTabsModule { }
