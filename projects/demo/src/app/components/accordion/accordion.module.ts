import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoAccordionModule } from '@evo-kit';
import { AccordionComponent } from './accordion.component';
import { AccordionRoutingModule } from './accordion-routing.module';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [AccordionComponent],
  imports: [AccordionRoutingModule, CommonModule, EvoAccordionModule, SharedModule]
})
export class AccordionModule {}
