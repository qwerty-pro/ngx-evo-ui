import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'accordion'
  },
  {
    path: 'accordion',
    loadChildren: () => import('./accordion/accordion.module').then((m) => m.AccordionModule)
  },
  {
    path: 'button',
    loadChildren: () => import('./button/button.module').then((m) => m.ButtonModule)
  },
  {
    path: 'checkbox',
    loadChildren: () => import('./checkbox/checkbox.module').then((m) => m.CheckboxModule)
  },
  {
    path: 'icon',
    loadChildren: () => import('./icon/icon.module').then((m) => m.IconModule)
  },
  {
    path: 'input',
    loadChildren: () => import('./input/input.module').then((m) => m.InputModule)
  },
  {
    path: 'radio-group',
    loadChildren: () => import('./radio-group/radio-group.module').then((m) => m.RadioGroupModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then((m) => m.TabsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule {}
