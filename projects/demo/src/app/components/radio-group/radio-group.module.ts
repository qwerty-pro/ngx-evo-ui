import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoRadioGroupModule } from '@evo-kit';
import { SharedModule } from '../../shared';

import { RadioGroupComponent } from './radio-group.component';
import { RadioGroupRoutingModule } from './radio-group-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RadioGroupComponent
  ],
  imports: [
    CommonModule,
    EvoRadioGroupModule,
    FormsModule,
    RadioGroupRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class RadioGroupModule { }
