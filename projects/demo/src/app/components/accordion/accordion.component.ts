import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { EvoAccordionPanel } from '@evo-kit';

@Component({
  selector: 'app-accordion',
  template: `
    <app-page-header pageTitle="Accordion" [descriptionTmpl]="descriptionTmpl"></app-page-header>
    <app-page-content [leftSectionTmpl]="leftSectionTmpl" [rightSectionTmpl]="rightSectionTmpl"></app-page-content>

    <ng-template #descriptionTmpl> </ng-template>
    
    <ng-template #leftSectionTmpl>
      <app-component-preview name="Basic" description="Basic usage of accordion">
        <evo-accordion [panels]="itemCollection1"></evo-accordion>
      </app-component-preview>
  
      <app-component-preview name="Disabled mode" description="Usage with disabled mode">
        <evo-accordion [panels]="itemCollection3"></evo-accordion>
      </app-component-preview>
    </ng-template>
    
    <ng-template #rightSectionTmpl>
      <app-component-preview name="Accordion Mode" description="Usage with enabled accordion mode">
        <evo-accordion [panels]="itemCollection2" [accordionMode]="true"></evo-accordion>
      </app-component-preview>
  
      <app-component-preview name="Borderless Mode" description="Usage with enabled borderless mode">
        <evo-accordion [panels]="itemCollection4" [borderLessMode]="true"></evo-accordion>
      </app-component-preview>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccordionComponent implements OnInit {
  @HostBinding('class.page-wrapper') readonly baseCss: boolean = true;

  itemCollection1: EvoAccordionPanel[] = [
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #1'
    },
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #2'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #3'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #4'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #5'
    }
  ];

  itemCollection2: EvoAccordionPanel[] = [
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #1'
    },
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #2'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #3'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #4'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #5'
    }
  ];
  
  itemCollection3: EvoAccordionPanel[] = [
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: true,
      header: 'Evo-accordion header #1'
    },
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #2'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: true,
      header: 'Evo-accordion header #3'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #4'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #5'
    }
  ];
  
  itemCollection4: EvoAccordionPanel[] = [
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #1'
    },
    {
      active: true,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #2'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #3'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #4'
    },
    {
      active: false,
      content: `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi aut distinctio doloremque,
        dolores eius ex incidunt iusto maiores, minima minus, natus nostrum nulla officiis pariatur porro quae qui
        quia quidem quo rem sequi temporibus unde ut voluptates? Consequatur cum eaque error laboriosam molestiae
        neque possimus provident repellat similique, voluptas?
      `,
      disabled: false,
      header: 'Evo-accordion header #5'
    }
  ];
  
  constructor() {}

  ngOnInit(): void {}
}
