import { Injectable } from '@angular/core';
import { EvoUISizes } from './common-ui.models';
import { EvoButtonColors, EvoButtonGroupColors } from '../button/button.models';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  static s4(): string {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  
  constructor() { }
  
  public getButtonColorClass(color: EvoButtonColors | EvoButtonGroupColors): string {
    switch (color) {
      case EvoButtonColors.PRIMARY:
        return 'primary';
      case EvoButtonColors.SECONDARY:
        return 'secondary';
      case EvoButtonColors.TERTIARY:
        return 'tertiary';
      case EvoButtonColors.ERROR:
        return 'error';
      case EvoButtonColors.SUCCESS:
        return 'success';
      case EvoButtonColors.GHOST:
        return 'ghost';
      case EvoButtonColors.INFO:
        return 'info';
      case EvoButtonColors.WARNING:
        return 'warning';
      default:
        return '';
    }
  }
  
  public generateUniqId(): string {
    return `${new Date().getTime()}-${UtilsService.s4()}-${UtilsService.s4()}-${UtilsService.s4()}-${UtilsService.s4()}`;
  }
  
  public getSizeClass(size: EvoUISizes): string {
    switch (size) {
      case EvoUISizes.SMALL:
        return 'sm';
      case EvoUISizes.LARGE:
        return 'lg';
      default:
        return 'md';
    }
  }
}
