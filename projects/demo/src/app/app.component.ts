import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { environment } from '../environments/environment';
import { ILinkItem, IMenuItem, LinkEnum } from './shared';
import packageJson from 'package.json';

@Component({
  selector: 'app-root',
  template: `
    <header class="header">
      <a class="link" routerLink="intro">
        <h4 class="evo-heading">{{ title }}</h4>
      </a>
      <div class="app-version">v{{ appVersion }}</div>
    </header>
    <app-left-nav [menuItems]="menuItems"></app-left-nav>
    <section class="content">
      <router-outlet></router-outlet>
    </section>
    <footer class="footer">
      <div>Made with ❤️ by Tony15025</div>
      <div class="footer__links">
        <ng-container *ngFor="let link of links">
          <evo-icon class="link" [name]="link.iconName" (click)="openSourcePage(link.source)"></evo-icon>
        </ng-container>
      </div>
    </footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  @HostBinding('class.base-layout') readonly baseCss: boolean = true;

  title = 'Evo UI';

  appVersion: string | number = packageJson.version;

  menuItems: IMenuItem[] = [
    { name: 'Accordion', link: 'components/accordion' },
    { name: 'Button', link: 'components/button' },
    { name: 'Checkbox', link: 'components/checkbox' },
    { name: 'Icon', link: 'components/icon' },
    { name: 'Input', link: 'components/input' },
    { name: 'Radio group', link: 'components/radio-group' },
    { name: 'Tabs', link: 'components/tabs' }
  ];

  links: ILinkItem[] = [
    { iconName: 'angular', source: LinkEnum.ANGULAR },
    { iconName: 'gitlab', source: LinkEnum.GITLAB }
  ];
  
  openSourcePage(sourceType: LinkEnum): void {
    window.open(environment.links[sourceType], '_blank');
  }
}
