export enum TextInputTypes {
  EMAIL = 'email',
  PASSWORD = 'password',
  TEXT = 'text'
}
