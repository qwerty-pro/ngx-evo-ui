import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvoIconModule } from '../icon';
import { UtilsModule } from '../@utils';

import { ButtonComponent } from './button.component';
import { ButtonGroupComponent } from './button-group.component';

const declarations: any[] = [
  ButtonComponent,
  ButtonGroupComponent
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    EvoIconModule,
    UtilsModule
  ],
  exports: [...declarations]
})
export class EvoButtonModule { }
