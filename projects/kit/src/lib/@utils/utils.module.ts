import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabelTemplateDirective } from './label-template.directive';
import { UtilsService } from './utils.service';


const declarations: any[] = [
  LabelTemplateDirective
];

@NgModule({
  declarations,
  imports: [CommonModule],
  providers: [UtilsService],
  exports: [...declarations]
})
export class UtilsModule {}
