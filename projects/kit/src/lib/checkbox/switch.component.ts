import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EvoUISizes, UtilsService } from '../@utils';


@Component({
  selector: 'evo-switch',
  template: `
    <input class="evo-switch__input" type="checkbox"
           [disabled]="disabled"
           [id]="switchId"
           [(ngModel)]="model"
           (ngModelChange)="onChange($event)"
    >
    <label class="evo-switch__label" for="{{ switchId }}">
      <ng-content></ng-content>
    </label>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true
    }
  ]
})
export class SwitchComponent implements ControlValueAccessor {
  @HostBinding('class.evo-switch') private readonly baseCss: boolean = true;
  
  @Input() disabled: boolean = false;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input()
  set model(value: boolean) {
    this._value = value;
  }
  
  get model(): boolean {
    return this._value;
  }
  
  get switchId(): string {
    return this._switchId;
  }
  
  @Output() modelChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @HostBinding('class') get additionalClasses(): string {
    return `evo-switch-${this._utilsService.getSizeClass(this.size)}`;
  }
  
  private readonly _switchId: string;
  private _value: boolean = false;
  private _onChange: (value: boolean) => void = () => {};
  private _onTouched: () => void = () => {};
  
  constructor(
    private readonly _utilsService: UtilsService
  ) {
    this._switchId = this._utilsService.generateUniqId();
  }

  writeValue(value: boolean): void {
    this._value = value;
  }
  
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChange(value: boolean): void {
    this._value = value;

    this._onChange(this._value);
    this._onTouched();
    this.modelChange.emit(this._value);
  }
}
