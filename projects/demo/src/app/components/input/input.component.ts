import { ChangeDetectionStrategy, Component, HostBinding, OnInit } from '@angular/core';
import { EvoRadioGroup, EvoUISizes } from '@evo-kit';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-input',
  template: `
    <app-page-header pageTitle="Input"></app-page-header>
    <app-page-content [mainSectionTmpl]="mainSectionTmpl"></app-page-content>
    
    <ng-template #mainSectionTmpl>
      <app-component-preview name="Basic" description="Basic usage of text input">
        <evo-text-input [(model)]="model1" (modelChange)="changeModel1($event)" placeholder="Enter your text..."></evo-text-input>
      </app-component-preview>
  
      <app-component-preview name="Disable mode" description="Usage input with disable mode">
        <evo-text-input [disabled]="true" [model]="model1"></evo-text-input>
      </app-component-preview>
  
      <app-component-preview name="Text input sizes" description="Input sizes">
        <div class="column-flex">
          <ng-container *ngFor="let input of textInputs">
            <evo-text-input [(model)]="input.model"
                            [placeholder]="input.placeholder"
                            [size]="input.size"
            ></evo-text-input>
          </ng-container>
        </div>
      </app-component-preview>
      
      <app-component-preview name="Password mode" description="Usage input with password mode">
        <div class="column-flex">
          <evo-text-input [model]="model1" placeholder="Enter your login"></evo-text-input>
          <evo-text-input [model]="model2" placeholder="Enter your password" [passwordMode]="true"></evo-text-input>
        </div>
      </app-component-preview>
  
      <app-component-preview name="Prefix and suffix" description="Usage with prefix/suffix icon">
        <div class="column-flex">
          <ng-container *ngFor="let input of inputs1">
            <evo-text-input [model]="input.model"
                            [placeholder]="input.placeholder"
                            [prefix]="input?.prefix || ''"
                            [suffix]="input?.suffix || ''"
            ></evo-text-input>
          </ng-container>
        </div>
      </app-component-preview>
      
      <app-component-preview name="Number input" description="Basic usage of number input">
        <evo-number-input [(model)]="numberModel1" (modelChange)="changeNumberValue1($event)"></evo-number-input>
      </app-component-preview>
      
      <app-component-preview name="Number input sizes" description="Sizes: small, medium (default), large">
        <div class="column-flex">
          <ng-container *ngFor="let input of numberInputs">
            <evo-number-input [(model)]="input.model"
                              [placeholder]="input.placeholder"
                              [size]="input.size"
            ></evo-number-input>
          </ng-container>
        </div>
      </app-component-preview>
      
      <app-component-preview name="Textarea" description="Basic usage of textarea">
        <evo-textarea
          maxLength="80"
          placeholder="Enter your text here..."
          [required]="true"
          [(model)]="textareaModel1"
          (modelChange)="changeTextareaModel1($event)"
        ></evo-textarea>
      </app-component-preview>
      
      <app-component-preview name="Reactive form" description="Usage components in reactive forms">
        {{ form.value | json }}
        <br>
        <form class="demo-form" [formGroup]="form">
          <evo-text-input formControlName="title" placeholder="Title"></evo-text-input>
          
          <evo-textarea formControlName="description" placeholder="Description"></evo-textarea>
          
          <evo-number-input formControlName="number" placeholder="Number"></evo-number-input>
        </form>
      </app-component-preview>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements OnInit {
  @HostBinding('class.page-wrapper') private readonly baseCss: boolean = true;

  model1: string = '';
  model2: string = '';
  
  numberModel1: number | null = null;
  
  textareaModel1: string = '';

  textInputs: { model: string, placeholder: string, size: EvoUISizes }[] = [
    { model: '', placeholder: 'Small input', size: EvoUISizes.SMALL },
    { model: '', placeholder: 'Medium input', size: EvoUISizes.MEDIUM },
    { model: '', placeholder: 'Large input', size: EvoUISizes.LARGE }
  ];

  numberInputs: { model: number | null, placeholder: string, size: EvoUISizes }[] = [
    { model: null, placeholder: 'Small input', size: EvoUISizes.SMALL },
    { model: null, placeholder: 'Medium input', size: EvoUISizes.MEDIUM },
    { model: null, placeholder: 'Large input', size: EvoUISizes.LARGE }
  ];
  
  inputs1: { model: string, placeholder: string, prefix?: string, suffix?: string }[] = [
    { model: '', placeholder: 'Enter your text...', prefix: 'lock' },
    { model: '', placeholder: 'Enter your text...', suffix: 'edit' },
    { model: '', placeholder: 'Enter your text...', prefix: 'mail', suffix: 'send' },
  ];

  form: UntypedFormGroup;

  constructor(private readonly _fb: UntypedFormBuilder) {
    this.form = this._fb.group({
      title: new UntypedFormControl('', [Validators.required]),
      description: new UntypedFormControl(''),
      number: new UntypedFormControl(null, [Validators.required]),
      gender: new UntypedFormControl('')
    });
  }

  ngOnInit(): void {
    console.log(this);
  }
  
  changeModel1(value: string) {
    console.log(this.model1, value);
  }
  
  changeNumberValue1(value: number | null) {
    console.log(value);
  }
  
  changeTextareaModel1(value: string) {
    console.log(value);
  }
}
