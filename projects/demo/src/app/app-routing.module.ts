import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'intro'
  },
  {
    path: 'components',
    loadChildren: () => import('./components/components.module').then((m) => m.ComponentsModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./intro/intro.module').then((m) => m.IntroModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
