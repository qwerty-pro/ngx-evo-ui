import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { catchError, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { EvoIconService } from './icon.service';

@Component({
  selector: 'evo-icon',
  template: `
    <div class="evo-icon" #iconWrapper></div>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent implements OnInit, OnChanges {
  @Input() name: string = '';
  @ViewChild('iconWrapper', { static: true }) readonly iconWrapper!: ElementRef<HTMLElement>;

  constructor(private readonly iconService: EvoIconService) {}

  ngOnInit(): void {
    if (this.name) {
      this.getIconByName(this.name);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { name }: SimpleChanges = changes;
    
    if (name && !name.isFirstChange()) {
      this.getIconByName(name.currentValue);
    }
  }
  
  private getIconByName(name: string): void {
    this.iconService
      .getIconByName(name)
      .pipe(
        take(1)
      )
      .subscribe((icon: string) => this.iconWrapper.nativeElement.innerHTML = icon);
  }
}
