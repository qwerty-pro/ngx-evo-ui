import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { EvoUISizes, UtilsService } from '../@utils';
import { EvoButtonGroup, EvoButtonGroupColors, EvoButtonGroupItem } from './button.models';

@Component({
  selector: 'evo-button-group',
  template: `
    <ng-container *ngFor="let button of buttons">
      <button class="evo-button-group__button"
              [class.active]="button.active"
              [disabled]="button.disabled"
              (click)="onClick(button)">
        <evo-icon *ngIf="button.iconName" [name]="button.iconName"></evo-icon>
        <ng-container *ngIf="button.label">
          <ng-container evoLabelTemplate [label]="button.label"></ng-container>
        </ng-container>
      </button>
    </ng-container>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonGroupComponent implements OnInit {
  @HostBinding('class.evo-button-group') private readonly baseCss: boolean = true;
  
  @Input() color: EvoButtonGroupColors = EvoButtonGroupColors.PRIMARY;
  @Input() size: EvoUISizes = EvoUISizes.MEDIUM;
  @Input()
  set buttons(value: EvoButtonGroup) {
    this._buttons = [...value];
  }
  @Output() onButtonToggle: EventEmitter<void> = new EventEmitter<void>();
  
  @HostBinding('class') get additionalClasses() {
    return `
      evo-button-${this._utilsService.getButtonColorClass(this.color)}
      evo-button-${this._utilsService.getSizeClass(this.size)}
    `;
  }
  
  get buttons(): EvoButtonGroup {
    return this._buttons;
  }
  
  private _buttons: EvoButtonGroup = [];
  
  constructor(private readonly _utilsService: UtilsService) { }

  ngOnInit(): void {
  }
  
  onClick(button: EvoButtonGroupItem): void {
    button.active = !button.active;
    button.onClick(button);
  }
}
