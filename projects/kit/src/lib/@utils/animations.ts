import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from '@angular/animations';

export const slideInOutAnimation: AnimationTriggerMetadata = trigger('slideInOut', [
  state('in', style({
    overflow: 'hidden',
    height: '*',
  })),
  state('out', style({
    overflow: 'hidden',
    height: 0
  })),
  transition('in <=> out', animate('500ms ease-in-out'))
]);
